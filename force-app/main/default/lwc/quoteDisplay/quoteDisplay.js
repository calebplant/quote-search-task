import { LightningElement, api } from 'lwc';
import { loadStyle } from 'lightning/platformResourceLoader';
import myQuoteDisplayCss from '@salesforce/resourceUrl/quoteDisplayCss';

export default class QuoteDisplay extends LightningElement {

    @api quoteData;
    @api visibleNestedChildren;
    @api visibleChildren;

    activeSections = [];
    activeChildSections = [];
    activeNestedChildSections = [];

    renderedCallback()
    {
        console.log('QuoteDisplay :: renderedCallback');
        
        // Load CSS
        Promise.all([
            loadStyle(this, myQuoteDisplayCss)
        ]);
        console.log('quoteData:');
        console.log(JSON.parse(JSON.stringify(this.quoteData)));
        
    }

    get accordianData()
    {
        console.log('QuoteDisplay :: get accordianData');
        
        // Filter out children/nested children that were not selected and set empty children values to undefined
        let result = [];
        result = this.quoteData.map(eachParent => {
            if (eachParent.children.length == 0) {
                return {...eachParent, children: undefined};
            }
            // Filter out non-selected nested children
            let childData = eachParent.children.map(eachChild => {
                // console.log('eachChild');
                // console.log(JSON.parse(JSON.stringify(eachChild)));
                    if(eachChild.children) {
                        if(eachChild.children.length > 0) {
                            // Filter out non-selected nested children
                            let nestedChildData = eachChild.children.filter(eachNestedChild => this.visibleNestedChildren.includes(eachNestedChild.Id));
                            return {...eachChild, children: nestedChildData};
                        }
                    }
                    return {...eachChild, children: undefined};
                });
            // Filter out non-selected children
            childData = childData.filter(eachChild => this.visibleChildren.includes(eachChild.Id));
            return {...eachParent, children: childData};
   });

        console.log('result:');
        console.log(JSON.parse(JSON.stringify(result)));

        return result;
    }

    handleToggleSection(event) {

    }

    handleChildToggleSelection(event) {

    }

    handleNestedChildToggleSelection(event) {
        
    }
}