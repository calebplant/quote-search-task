import { LightningElement } from 'lwc';

export default class QuoteSearchBar extends LightningElement {
    searchTerm = '';

    handleChange(event)
    {
        this.searchTerm = event.detail.value;
    }

    handleSearch()
    {
        console.log('QuoteSearchBar :: handleSearch');
        console.log('searchTerm: ' + this.searchTerm);

        this.dispatchEvent(new CustomEvent('submitsearch', {detail: this.searchTerm}));
    }

    handleReset()
    {
        console.log('QuoteSearchBar :: handleReset');

        this.dispatchEvent(new CustomEvent('resetform'));
    }
}