import { LightningElement, api } from 'lwc';
import { loadStyle } from 'lightning/platformResourceLoader';
import myChildSelectModalCss from '@salesforce/resourceUrl/childSelectModalCss';

const IGNORED_CHILD_TYPES = ['QuoteDocument'];

export default class ChildSelectModal extends LightningElement {

    @api childData;
    selected;

    get options()
    {
        let options = [];
        this.childData.forEach(eachChild => {
            if(!IGNORED_CHILD_TYPES.includes(eachChild.objectDisplayName)) { // Skip certain object types to avoid duplicates
                let displayStr = `${eachChild.parentDisplayName} ||   ${eachChild.displayName} (${eachChild.objectDisplayName})`;
                options.push( {label: displayStr, value: eachChild.Id} );
            }

        });

        return options;
    }

    get parentRecordNames()
    {
        let parents = new Set();
        this.childData.forEach(eachChild => {
            if(!IGNORED_CHILD_TYPES.includes(eachChild.objectDisplayName)) { // Skip certain object types to avoid duplicates
                parents.add(eachChild.parentDisplayName);
            }
        });
        return [...parents].join(', ');
    }

    renderedCallback()
    {
        console.log('childSelectModal :: renderedCallback');
        
        // Load CSS
        Promise.all([
            loadStyle(this, myChildSelectModalCss)
        ]);
    }

    handleChange(event)
    {
        this.selected = event.detail.value;
    }

    handleModalCancel()
    {
        this.dispatchEvent(new CustomEvent('modalcancel'));
    }

    handleModalSubmit()
    {
        console.log('selected: ' + this.selected);
        
        this.dispatchEvent(new CustomEvent('modalsubmit', {detail: '' + this.selected}));
    }
}