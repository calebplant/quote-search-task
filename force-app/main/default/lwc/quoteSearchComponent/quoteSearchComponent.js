import { LightningElement } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getQuotesAndChildrenByName from '@salesforce/apex/QuoteSearchController.getQuotesAndChildrenByName';
import getChildrenByPassedIds from '@salesforce/apex/QuoteSearchController.getChildrenByPassedIds';

const IGNORED_CHILD_TYPES = ['QuoteDocument'];

export default class QuoteSearchComponent extends LightningElement {


    searchTerm;
    showModal=false;
    showQuotes=false;
    popupIsNested = false;
    error;

    passedData;
    finalData;
    visibleChildren;
    visibleNestedChildren;

    // Handlers
    handleSubmitSearch(event)
    {
        console.log('QuoteSearchComponent :: handleSubmitSearch');

        this.searchTerm = event.detail;
        getQuotesAndChildrenByName({searchName: this.searchTerm})
        .then(response => {
            console.log('SEARCH RESPONSE: ');
            console.log(response);
            
            if(response.length > 0) {
                this.popupIsNested = false;
                this.finalData = response;
                this.passedData = this.flattenChildData(response);
                if(this.passedData.length == 0) { // No child records associated with found quotes
                    this.showQuotes = true;
                } else {
                    this.showModal = true; // Show modal for selecting children
                }
            } else { // No results
                // Show error Toast
                this.dispatchEvent(new ShowToastEvent({
                    title: 'No Quotes found',
                    message: 'No quotes found! Please try a different name.',
                    variant: 'error'
                    })
                )
            }
        }).catch(error => {
            console.log('ERROR on getQuotesAndChildrenByName');
            console.log(error);
        })
    }

    handleReset()
    {
        console.log('QuoteSearchComponent :: handleReset')
        this.showQuotes=false;
        this.popupIsNested = false;
    
        this.passedData = [];
        this.finalData = [];
        this.visibleChildren = [];
        this.visibleNestedChildren = [];
    }

    handleCancelModal(event)
    {
        console.log('QuoteSearchComponent :: handleCancelModal');
        
        this.showModal = false;
        this.popupIsNested = false;
    }

    handleSubmitModal(event)
    {
        console.log('QuoteSearchComponent :: handleSubmitModal :: nested = ' + this.popupIsNested);

        if(this.popupIsNested) {
            this.handleNestedSubmit(event.detail);
        } else {
            this.visibleChildren = event.detail.split(',');
            this.fetchNestedChildren(event.detail);
        }
    }

    handleNestedSubmit(nestedChildIds)
    {
        console.log('QuoteSearchComponent :: handleNestedSubmit');
        this.visibleNestedChildren = nestedChildIds.split(',');
        this.showModal = false;
        this.showQuotes = true;
    }

    // Utility
    flattenChildData(data)
    {
        console.log('QuoteSearchComponent :: flattenChildData');
        
        // Create a flat array of all the children returned to show in the select menu
        let result = [];
        data.forEach(eachParent => {
            if(eachParent.children.length > 0) {
                result = result.concat(eachParent.children);
            }
        });
        console.log('result:');
        console.log(result);
        return result;
    }

    flattenNestedChildData(data)
    {
        console.log('QuoteSearchComponent :: flattenNestedChildData');

        // Create a flat array of all nested children returned to show in the select menu
        let result = [];
        data.forEach(eachParent => {
            if(eachParent.children.length > 0) {
                eachParent.children.forEach(eachChild => {
                    if(eachChild.children) {
                        result = result.concat(eachChild.children);
                    }
                })
            }
        });
        console.log('result:');
        console.log(result);
        return result;
    }

    fetchNestedChildren(selectedIds)
    {
        // Get nested children of children selected by user in popup menu
        getChildrenByPassedIds({commaSeparatedIds: '' + selectedIds, nodeJson: JSON.stringify(this.finalData)})
        .then(response => {
            console.log('RESPONSE:');
            console.log(response);
            this.finalData = response;
            this.passedData = this.flattenNestedChildData(response);
            if(this.passedData.length == 0) { // No nested children records found
                this.showModal = false;
                this.showQuotes = true;
            } else {
                this.popupIsNested = true;
            }
        }).catch(error => {
            console.log('ERROR on getChildrenByPassedIds:');
            console.log(error);
            this.showModal = false;
        });
    }
}