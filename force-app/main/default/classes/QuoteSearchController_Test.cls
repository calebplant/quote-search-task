@isTest
public class QuoteSearchController_Test {

    private static String QUOTE_1_NAME = 'quote1';
    private static String QUOTE_2_NAME = 'quote2';
    private static Integer NUM_CHILDREN_QUOTE_1 = 3;
    private static Integer NUM_NESTED_CHILDREN_QUOTE_1 = 1;
    private static String CHILD_1_NAME = 'child1';
    private static String CHILD_2_NAME = 'child2';
    private static String NESTED_CHILD_1_NAME = 'nested1';
 
    @TestSetup
    static void makeData(){
        // Opportunity
        Opportunity opp = new Opportunity(Name='opp1', CloseDate=date.today(), StageName='Legal');
        insert opp;
        // Quotes
        Quote quote1 = new Quote(Name=QUOTE_1_NAME, OpportunityId=opp.Id);
        Quote quote2 = new Quote(Name=QUOTE_2_NAME, OpportunityId=opp.Id);
        insert quote1;
        insert quote2;
        // Attachment on quote1
        ContentVersion cv = new ContentVersion();
        cv.Title='attachment1';
        cv.PathOnClient = '/attachment1.txt';
        cv.FirstPublishLocationId = quote1.Id;
        cv.VersionData = EncodingUtil.base64Decode('data here');
        cv.IsMajorVersion = true;
        insert cv;
        // Children on Quote 1
        Child_of_Quote__c child1 = new Child_of_Quote__c(Name=CHILD_1_NAME, Quote__c=quote1.Id);
        Child_of_Quote__c child2 = new Child_of_Quote__c(Name=CHILD_2_NAME, Quote__c=quote1.Id);
        insert child1;
        insert child2;
        // Nested child on child1
        Nested_Child_of_Quote__c nested1 = new Nested_Child_of_Quote__c(Name=NESTED_CHILD_1_NAME, Child_of_Quote__c=child1.Id);
        insert nested1;
    }

    @isTest
    static void doesGetQuotesAndChildrenByNameReturnEmptyIfNoChildren()
    {
        Test.startTest();
        List<QuoteSearchResponse> response = QuoteSearchController.getQuotesAndChildrenByName(QUOTE_2_NAME);
        Test.stopTest();

        // System.debug('response: ' + response);
        System.assertEquals(0, response[0].children.size());
    }

    @isTest
    static void doesGetQuotesAndChildrenByNameReturnParentWithChildren()
    {
        Test.startTest();
        List<QuoteSearchResponse> result = QuoteSearchController.getQuotesAndChildrenByName(QUOTE_1_NAME);
        Test.stopTest();

        // System.debug('response: ' + response);
        System.assertEquals(NUM_CHILDREN_QUOTE_1, result[0].children.size());
    }

    @isTest
    static void doesGetChildrenByPassedIdsReturnHierarchy()
    {
        List<QuoteSearchResponse> quotesWithChildren = QuoteSearchController.getQuotesAndChildrenByName(QUOTE_1_NAME);
        String nodeJson = (String)JSON.serialize(quotesWithChildren);

        List<Child_of_Quote__c> quoteChildren = [SELECT Id 
                                            FROM Child_of_Quote__c 
                                            WHERE Name = :CHILD_1_NAME];
        String selectedChildId = '' + quoteChildren[0].Id;
        System.debug('selectedChildId' + selectedChildId);
        Test.startTest();
        List<QuoteSearchResponse> result = QuoteSearchController.getChildrenByPassedIds(selectedChildId, nodeJson);
        Test.stopTest();

        System.debug('response:');
        for(QuoteSearchResponse eachRes : result) {
            System.debug(eachRes);
        }
        System.assertEquals(NUM_NESTED_CHILDREN_QUOTE_1, result[0].children[0].children.size());
    }

    @isTest
    static void doesGetChildrenByPassedIdsReturnOriginalHierarchyIfNoChildren()
    {
        List<QuoteSearchResponse> quotesWithChildren = QuoteSearchController.getQuotesAndChildrenByName(QUOTE_1_NAME);
        String nodeJson = (String)JSON.serialize(quotesWithChildren);

        List<Child_of_Quote__c> quoteChildren = [SELECT Id 
                                            FROM Child_of_Quote__c 
                                            WHERE Name = :CHILD_2_NAME];
        String selectedChildId = '' + quoteChildren[0].Id;
        System.debug('selectedChildId' + selectedChildId);
        Test.startTest();
        List<QuoteSearchResponse> result = QuoteSearchController.getChildrenByPassedIds(selectedChildId, nodeJson);
        Test.stopTest();

        System.debug('response:');
        for(QuoteSearchResponse eachRes : result) {
            System.debug(eachRes);
        }
        for(QuoteSearchResponse eachQuote : result) {
            for(QuoteSearchResponse eachChild : eachQuote.children) {
                System.assertEquals(null, eachChild.children);
            }
        }
        // System.assertEquals(0, result[0].children[0].children.size());
    }
}
