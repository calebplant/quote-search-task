public without sharing class Constants {
    public static final List<String> UI_API_COMPATIBLE_OBJECTS;

    static {
        StaticResource static_resource = [SELECT Id, Body
                                  FROM StaticResource 
                                  WHERE Name = 'Supported_UI_Objects'
                                  LIMIT 1];
        UI_API_COMPATIBLE_OBJECTS = static_resource.Body.toString().split(',');
    } 
}