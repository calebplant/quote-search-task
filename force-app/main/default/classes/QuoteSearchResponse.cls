public class QuoteSearchResponse {
    @AuraEnabled
    public String displayName;
    @AuraEnabled
    public String objectDisplayName;
    @AuraEnabled
    public String parentDisplayName;
    @AuraEnabled
    public string parentId;
    @AuraEnabled
    public Id Id;
    
    @AuraEnabled
    public List<QuoteSearchResponse> children;

    public QuoteSearchResponse(Id id) {
        this.Id = id;
    }
}
