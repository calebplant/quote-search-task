public without sharing class QuoteSearchController {
    
    /*
        Returns a list of Quote data and data for their related child records
    */
    @AuraEnabled(cacheable=true)
    public static List<QuoteSearchResponse> getQuotesAndChildrenByName(String searchName){
        // Get Quote's children's label, api name, and the field name of the relationship from quote to the child
        List<ChildRelationshipsWrapper> childRelationshipNames = RelationshipUtils.getChildObjectRelationshipDetails('Quote');

        // Map the wrapper by the name of the child => Quote field (ex: Account => {Contacts: [Contact Wrapper]} )
        Map<String, ChildRelationshipsWrapper> childRelWrapperByRelationshipName = new Map<String, ChildRelationshipsWrapper>();
        for(ChildRelationshipsWrapper eachWrapper : childRelationshipNames) {
            childRelWrapperByRelationshipName.put(eachWrapper.relationshipName, eachWrapper);
        }

        // Build subqueries for each Child object type to query
        String subQueries = '';
        for(ChildRelationshipsWrapper eachChild : childRelationshipNames) {
            subQueries += ', (SELECT Id, ' + eachChild.displayName + ' FROM ' + eachChild.relationshipName + ')';
        }

        // Submit query
        String likeClause = '%' + searchName + '%'; 
        String query = 'SELECT Id, Name' + subQueries + ' FROM Quote WHERE Name LIKE :likeClause';
        System.debug('Query: ' + query);
        System.debug('likeClause: ' + likeClause);
        List<Quote> quoteResult = Database.query(query);
        // Map query results by Id
        Map<Id, Quote> quoteById = new Map<Id, Quote>(quoteResult);
        // Return if no quotes found
        if(quoteById.size() == 0) { return new List<QuoteSearchResponse>();}

        // Iterate through query results and build a response for each one
        // List<QuoteSearchResponse> quoteData = new List<QuoteSearchResponse>();
        Map<Id, QuoteSearchResponse> quoteResponseByQuoteId = new Map<Id, QuoteSearchResponse>();
        for(Quote eachQuote : quoteById.values()) {
            // Iterate through object's fields to find child relationships
            List<QuoteSearchResponse> quoteChildInfo = new List<QuoteSearchResponse>();
            Map<String, Object> fieldMap =  eachQuote.getPopulatedFieldsAsMap();
            for(String eachField : fieldMap.keySet()) {
                if(childRelWrapperByRelationshipName.keySet().contains(eachField)) { // If field is child relationship field (ex: QuoteLineItems)
                    // Iterate through child objects of that type
                    for(sObject eachChild : (List<SObject>)fieldMap.get(eachField)) {
                        ChildRelationshipsWrapper currentChildWrapper = childRelWrapperByRelationshipName.get(eachField);
                        // Build response for each child
                        QuoteSearchResponse newResponse = new QuoteSearchResponse(eachChild.Id);
                        newResponse.displayName = (String)eachChild.get(currentChildWrapper.displayName); // ex: Name
                        newResponse.objectDisplayName = currentChildWrapper.apiName; // ex: Contact
                        newResponse.parentDisplayName = (String)eachQuote.get(currentChildWrapper.parentDisplayName);  //ex: Account
                        newResponse.parentId = eachQuote.Id;
                        quoteChildInfo.add(newResponse);
                    }
                }
            }

            // Add response for the current Quote
            QuoteSearchResponse quoteRes = new QuoteSearchResponse(eachQuote.Id);
            quoteRes.displayName = eachQuote.Name;
            quoteRes.objectDisplayName = 'Quote';
            quoteRes.children = quoteChildInfo;

            quoteResponseByQuoteId.put(eachQuote.Id, quoteRes);
            // quoteData.add(quoteRes);
        }

        /* Add content documents to quotes */
        List<ContentDocumentLink> docLinks = [SELECT ContentDocumentId, LinkedEntityId, ContentDocument.Title, ContentDocument.FileType
                                                FROM ContentDocumentLink
                                                WHERE LinkedEntityId IN :quoteById.keySet()];
        for(ContentDocumentLink eachDocLink : docLinks) {
            QuoteSearchResponse newResponse = new QuoteSearchResponse(eachDocLink.ContentDocumentId);
            newResponse.displayName = eachDocLink.ContentDocument.Title;
            newResponse.objectDisplayName = 'ContentDocument';
            newResponse.parentDisplayName = quoteById.get(eachDocLink.LinkedEntityId).Name;
            newResponse.parentId = eachDocLink.LinkedEntityId;
            QuoteSearchResponse currQuoteRes = quoteResponseByQuoteId.get(eachDocLink.LinkedEntityId);
            currQuoteRes.children.add(newResponse);
        }

        System.debug('result (quoteResponseByQuoteId.values()): ' + quoteResponseByQuoteId.values());
        return quoteResponseByQuoteId.values();
    }

    /* 
        Returns a list of hierarchies of Quote, child, and grandchild records

        * commaSeparatedIds - the Ids of the child objects that we need to query against
        * nodeJson - Exisitng hierarchy of Quote and child objects that we will add to

        Note: child refers to the Quote's child; nested child refers the to Quote's grandchild
    */
    @AuraEnabled(cacheable=true)
    public static List<QuoteSearchResponse> getChildrenByPassedIds(String commaSeparatedIds, String nodeJson){
        // Read passed data
        List<QuoteSearchResponse> nodeData = (List<QuoteSearchResponse>)JSON.deserialize(nodeJson, List<QuoteSearchResponse>.class);
        List<Id> passedIds = commaSeparatedIds.split(',');

        // Group passed Ids (children) by their object's Api name
        Map<String, List<Id>> recordIdsByApiName = RelationshipUtils.mapRecordsByObjectApiName(passedIds);
        // System.debug(recordIdsByApiName);

        /* Map nested child's relationship field name by their parent's api name by their grandparent's api name */
        Map<String, Map<String, ChildRelationshipsWrapper>> relationshipNamesByParentNameByGrandparentName = new Map<String, Map<String, ChildRelationshipsWrapper>>();
        for(String eachApiName : recordIdsByApiName.keySet()) { // iterate on children's api names
            // Get nested children's label, api name, and the field name of the relationship from quote to the child
            List<ChildRelationshipsWrapper> currentChildRelationshipNames = RelationshipUtils.getChildObjectRelationshipDetails(eachApiName);
            
            // Map the wrapper by the name of the nestedChild => Child field (ex: Account => {Contacts: [Contact Wrapper]} )
            Map<String, ChildRelationshipsWrapper> childRelWrapperByRelationshipName = new Map<String, ChildRelationshipsWrapper>();
            for(ChildRelationshipsWrapper eachWrapper : currentChildRelationshipNames) { // Iterate on child
                childRelWrapperByRelationshipName.put(eachWrapper.relationshipName, eachWrapper);
            }
            // Map new child map by grandparent's api name
            relationshipNamesByParentNameByGrandparentName.put(eachApiName, childRelWrapperByRelationshipName);
        }

        // Build child relationship wrappers for each object Api Name
        Map<String, List<ChildRelationshipsWrapper>> childRelationshipNamesByParentApiName = new Map<String, List<ChildRelationshipsWrapper>>();
        for(String eachApiName : recordIdsByApiName.keySet()) {
            childRelationshipNamesByParentApiName.put(eachApiName, RelationshipUtils.getChildObjectRelationshipDetails(eachApiName));
        }

        // Build queries for each object Api name
        Map<String, String> queryByApiName = new Map<String, String>();
        for(String eachApiName : childRelationshipNamesByParentApiName.keySet()) {
            String subQueries = '';
            for(ChildRelationshipsWrapper eachWrapper : childRelationshipNamesByParentApiName.get(eachApiName)) {
                subQueries += ', (SELECT Id, ' + eachWrapper.displayName + ' FROM ' + eachWrapper.relationshipName + ')';
            }
            List<Id> queryIds = recordIdsByApiName.get(eachApiName);
            String parentName = '';
            if(childRelationshipNamesByParentApiName.get(eachApiName).size() > 0) {
                System.debug(childRelationshipNamesByParentApiName.get(eachApiName));
                parentName = ', ' + childRelationshipNamesByParentApiName.get(eachApiName)[0].parentDisplayName;
            }
            String query = 'SELECT Id' + parentName + subQueries + ' FROM ' + eachApiName + ' WHERE Id IN :passedIds';
            queryByApiName.put(eachApiName, query);
        }

        // Run query for each child object type to get nested child records
        Map<String, List<sObject>> queryResultByObjectApiName = new Map<String, List<sObject>>();
        for(String eachApiName : queryByApiName.keySet()) {
            System.debug('Query: ' + queryByApiName.get(eachApiName));
            List<SObject> queryResult = Database.query(queryByApiName.get(eachApiName));
            System.debug('Query result: ' + queryResult);
            queryResultByObjectApiName.put(eachApiName, queryResult);
        }


        /* Iterate through query results and build responses */
        List<QuoteSearchResponse> childInfo = new List<QuoteSearchResponse>();
        Map<Id, List<QuoteSearchResponse>> nestedChildrenByParentId = new Map<Id, List<QuoteSearchResponse>>();
        // Iterate through child api names
        for(String eachApiName : queryResultByObjectApiName.keySet()) {
                // Iterate through query result for child
                for(sObject eachObj : queryResultByObjectApiName.get(eachApiName)) {
                // Iterate through fields on query result object
                Map<String, Object> fieldMap =  eachObj.getPopulatedFieldsAsMap();
                for(String eachField : fieldMap.keySet()) {
                    Boolean isChildRelationshipField = relationshipNamesByParentNameByGrandparentName.get(eachApiName).keySet().contains(eachField);
                    if(isChildRelationshipField) { // If field is child relationship field (ex: QuoteLineItems)
                        // Iterate through nested children
                        for(sObject eachChild : (List<SObject>)fieldMap.get(eachField)) {
                            ChildRelationshipsWrapper currentChildWrapper = relationshipNamesByParentNameByGrandparentName.get(eachApiName).get(eachField);
                            QuoteSearchResponse newResponse = new QuoteSearchResponse(eachChild.Id);
                            // Build response for each nested child
                            newResponse.displayName = (String)eachChild.get(currentChildWrapper.displayName); // ex: Name
                            newResponse.objectDisplayName = currentChildWrapper.apiName; // ex: Contact
                            newResponse.parentDisplayName = (String)eachObj.get(currentChildWrapper.parentDisplayName); // ex: Name
                            newResponse.parentId = eachObj.Id;
                            childInfo.add(newResponse);

                            // Add nested child to map 
                            if(nestedChildrenByParentId.get(newResponse.parentId) != null) {
                                List<QuoteSearchResponse> currResList = nestedChildrenByParentId.get(newResponse.parentId);
                                currResList.add(newResponse);
                                nestedChildrenByParentId.put(newResponse.parentId, currResList);
                            } else {
                                List<QuoteSearchResponse> resList = new List<QuoteSearchResponse>();
                                resList.add(newResponse);
                                nestedChildrenByParentId.put(newResponse.parentId, resList);
                            }
                        }
                    }
                }

            }
        }

        // Add nested children to existing quote children nodes
        System.debug('Updated Node Data...');
        for(QuoteSearchResponse eachParentNode : nodeData) {
            for(QuoteSearchResponse eachChildNode : eachParentNode.children) {
                eachChildNode.children = nestedChildrenByParentId.get(eachChildNode.Id);
            }
        }
        System.debug('result (nodeData): ' + nodeData);
        return nodeData;
    }
}
