public without sharing class RelationshipUtils {

    /* Groups passed Ids by their object's api name */
    public static Map<String, List<Id>> mapRecordsByObjectApiName(List<Id> passedIds) {
        Map<String, List<Id>> recordIdsByApiName = new Map<String, List<Id>>();
        for(Id eachId : passedIds) {
            String apiName = eachId.getSObjectType().getDescribe().getName();
            if(recordIdsByApiName.get(apiName) == null) {
                recordIdsByApiName.put(apiName, new List<Id>{eachId});
            } else {
                List<Id> updatedIdList = recordIdsByApiName.get(apiName);
                updatedIdList.add(eachId);
                recordIdsByApiName.put(apiName, updatedIdList);
            }
        }
        return recordIdsByApiName;
    }

    /* Returns a list of wrappers containing information about the passed object's */
    public static List<ChildRelationshipsWrapper> getChildObjectRelationshipDetails(String apiName)
    {
        System.debug('getChildObjectRelationshipDetails');
        List<ChildRelationshipsWrapper> childrenRelationshipDetails = new List<ChildRelationshipsWrapper>();
        
        SObjectType recordType = ((SObject)(Type.forName('Schema.'+apiName).newInstance())).getSObjectType(); 
        // Get ChildRelationship fields
        List<Schema.ChildRelationship> childRelationships = recordType.getDescribe().getChildRelationships();
        // Get name of passed object's "Name" field
        String parentDisplayName = getNameField(recordType);

        // Map relationship names
        for(Schema.ChildRelationship eachChild : childRelationships) {
            // Filter out objects not supported by the UI API or that the user can't access
            Boolean userCanAccess = eachChild.getChildSObject().getDescribe().isAccessible();
            Boolean isSupportedByUiApi = Constants.UI_API_COMPATIBLE_OBJECTS.contains('' + eachChild.getChildSObject());
            Boolean isCustom = eachChild.getChildSObject().getDescribe().isCustom();

            // Create wrappers and set relationship and API names
            if(userCanAccess && (isSupportedByUiApi || isCustom)) {
                if(eachChild.getRelationshipName() != null) {
                    ChildRelationshipsWrapper newWrapper = new ChildRelationshipsWrapper();
                    newWrapper.relationshipName = '' + eachChild.getRelationshipName(); // ex: Contacts
                    newWrapper.apiName = '' + eachChild.getChildSObject(); // ex: Contact
                    newWrapper.parentApiName = apiName; // ex: Account
                    newWrapper.parentDisplayName = parentDisplayName; // ex: Name
                    childrenRelationshipDetails.add(newWrapper);
                }
            }
        }

        // Get display field name for child record
        childrenRelationshipDetails = RelationshipUtils.getChildDisplayFields(childrenRelationshipDetails);

        return childrenRelationshipDetails;
    }

    /* Return the api name for the passed object's name field */
    public static String getNameField(SObjectType sObjectType)
    {
        String nameKey;

        Map<String, SObjectField> fieldsMap = sObjectType.getDescribe().fields.getMap();
        if (!fieldsMap.containsKey('Name') || !fieldsMap.get('Name').getDescribe().isNameField())
        {
            for ( SObjectField fieldToken : fieldsMap.values() ) {
                DescribeFieldResult fieldDescribe = fieldToken.getDescribe();
                if ( fieldDescribe.isNameField() ) {
                    return fieldDescribe.getName();
                }
            }
        }
        return 'Name';
    }

    /* Set displayname for each passed wrapper */
    public static List<ChildRelationshipsWrapper> getChildDisplayFields(List<ChildRelationshipsWrapper> childDetails) {
        // System.debug('getChildDisplayFields');
        List<String> apiNames = new List<String>();
        for(ChildRelationshipsWrapper eachChild : childDetails) {
            apiNames.add(eachChild.apiName);
        }
        // Query for object display fields
        List<FieldDefinition> displayNames = [SELECT QualifiedApiName, EntityDefinitionId, EntityDefinition.QualifiedApiName
                                                FROM FieldDefinition
                                                WHERE EntityDefinition.QualifiedApiName IN :apiNames
                                                AND IsNameField = TRUE];

        // Map display name by object api name
        Map<String, String> displayNameByApiName = new Map<String, String>();
        for(FieldDefinition eachFieldDef : displayNames) {
            if(isValidRecordId((String)eachFieldDef.EntityDefinitionId)) { // Custom object
                displayNameByApiName.put(eachFieldDef.EntityDefinition.QualifiedApiName, eachFieldDef.QualifiedApiName);
            } else { // Standard object
                displayNameByApiName.put(eachFieldDef.EntityDefinitionId, eachFieldDef.QualifiedApiName);
            }
        }

        // Set display name for each child wrapper
        for(ChildRelationshipsWrapper eachChild : childDetails) {
            eachChild.displayName = displayNameByApiName.get(eachChild.apiName);
        }

        return childDetails;
    }

    public static Boolean isValidRecordId(String value) {
        try {
            Id recordId = Id.valueOf(value);
            return True;
        } catch (Exception e) {
            return False;
        }
        
    }
}
